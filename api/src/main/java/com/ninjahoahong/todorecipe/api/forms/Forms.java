package com.ninjahoahong.todorecipe.api.forms;

import com.google.appengine.repackaged.com.google.common.collect.ImmutableList;
import com.google.appengine.repackaged.com.google.common.collect.ImmutableSet;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Forms {

    @NoArgsConstructor
    public static class RecipeCreationForm {
        @Getter(AccessLevel.PUBLIC)
        private String recipeName;

        @Getter(AccessLevel.PUBLIC)
        private List<String> tasksList;

        @Getter(AccessLevel.PUBLIC)
        private String authorKeyString;

        @Getter(AccessLevel.PUBLIC)
        private Set<String> categoriesSet = new HashSet<String>();

        @Getter(AccessLevel.PUBLIC)
        private String recipeColor;

        public RecipeCreationForm(
                String recipeName, List<String> tasksList,
                String authorKeyString, Set<String> pa_categoriesSet, String recipeColor) {
            this.recipeName = recipeName == null ? "" : recipeName;
            this.tasksList = tasksList == null ? null : ImmutableList.copyOf(tasksList);
            this.authorKeyString = authorKeyString;
            this.categoriesSet = ImmutableSet.copyOf(pa_categoriesSet);
            this.recipeColor = recipeColor;
        }
    }

    @NoArgsConstructor
    @AllArgsConstructor
    public static class CategoryCreationForm {
        @Getter(AccessLevel.PUBLIC)
        private String name;
        @Getter(AccessLevel.PUBLIC)
        private String parentCategoryName;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeleteResponseForm {
        @Getter(AccessLevel.PUBLIC)
        private boolean status;
        @Getter(AccessLevel.PUBLIC)
        private String message;
    }
}
