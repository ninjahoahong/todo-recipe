package com.ninjahoahong.todorecipe.api.constants;


public class Messages {
    public static final String INVALID_USER = "Invalid user";
    public static final String PASSIVE_USER_TYPE = "passive";
    public static final String ACTIVE_USER_TYPE = "active";
    public static final String USER_DELETE_SUCCESS_MESSAGE = "User has been deleted";
    public static final String USER_DELETE_FAIL_MESSAGE = "Failed to delete user";
    public static final String RECEIPT_DELETE_SUCCESS_MESSAGE = "Recipe has been deleted";
    public static final String RECEIPT_DELETE_FAIL_MESSAGE = "Failed to delete recipe";
}
