package com.ninjahoahong.todorecipe.api.helpers;

import com.googlecode.objectify.Key;
import com.ninjahoahong.todorecipe.api.entities.Category;
import com.ninjahoahong.todorecipe.api.entities.Recipe;
import com.ninjahoahong.todorecipe.api.entities.Task;
import com.ninjahoahong.todorecipe.api.entities.User;
import com.ninjahoahong.todorecipe.api.constants.General;
import com.ninjahoahong.todorecipe.api.services.OfyService;

public class Utilities {
    public static User findUserByKeyString(final String pa_key) {
        User user = (User) OfyService.ofy().load().key(Key.valueOf(pa_key)).now();
        return user;
    }

    public static User findUserByFacebookId(final String pa_facebookId) {
        User user = OfyService.ofy().load().type(User.class).filter("facebookId =", pa_facebookId).first().now();
        return user;
    }

    public static Recipe findRecipeByKeyString(final String pa_key) {
        Recipe recipe = (Recipe) OfyService.ofy().load().key(Key.valueOf(pa_key)).now();
        return recipe;
    }

    public static Category findCategoryByName(final String pa_name) {
        Category category = OfyService.ofy().load().type(Category.class).id(pa_name).now();
        return category;
    }

    public static Category findCategoryByKeyString(final String pa_categoryKeyString) {
        Category category = (Category) OfyService.ofy().load().key(Key.valueOf(pa_categoryKeyString)).now();
        return category;
    }

    public static void saveUser(User user) {
        OfyService.ofy().save().entity(user).now();
    }

    public static void delete(final String key) {
        OfyService.ofy().delete().key(Key.valueOf(key)).now();
    }

    public static void saveRecipe(Recipe recipe) {
        OfyService.ofy().save().entity(recipe).now();
    }

    public static void saveTask(Task task) {
        OfyService.ofy().save().entity(task).now();
    }

    public static void saveCategory(Category category) {
        OfyService.ofy().save().entity(category).now();
    }

    public static boolean isEmptyString(String s) {
        return s.trim().isEmpty();
    }
}

