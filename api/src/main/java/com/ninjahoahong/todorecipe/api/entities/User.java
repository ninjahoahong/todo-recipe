package com.ninjahoahong.todorecipe.api.entities;

import com.googlecode.objectify.annotation.Index;
import com.ninjahoahong.todorecipe.api.constants.General;
import lombok.Getter;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import lombok.Setter;

import java.util.UUID;

@Entity(name = "User")
@NoArgsConstructor
public class User {

    @Getter(AccessLevel.PUBLIC)
    @Id
    private String id;

    @Setter(AccessLevel.PUBLIC)
    @Getter(AccessLevel.PUBLIC)
    private String userStatus;

    @Getter(AccessLevel.PUBLIC)
    private String email;
    public void setEmail(final String pa_email) {
        if(pa_email == null){
            email = General.DEFAULT_EMAIL;
        } else {
            email = pa_email;
        }
    }

    @Setter(AccessLevel.PUBLIC)
    @Getter(AccessLevel.PUBLIC)
    private String name;

    @Setter(AccessLevel.PUBLIC)
    @Getter(AccessLevel.PUBLIC)
    @Index
    private String facebookId;

    @Setter(AccessLevel.PUBLIC)
    @Getter(AccessLevel.PUBLIC)
    private String facebookAccessToken;

    public static class Builder {
        private String userDeviceId;

        private String userName;
        public Builder addUserName(String pa_name) {
            userName = pa_name;
            return this;
        }

        private String userEmail;
        public Builder addUserEmail(String pa_mail) {
            userEmail = pa_mail;
            return this;
        }

        private String userFacebookAccessToken;
        public Builder addAccessToken(String pa_token) {
            userFacebookAccessToken = pa_token;
            return this;
        }

        private String userFacebookId;
        public Builder addUserFacebookId(String pa_facebookId) {
            userFacebookId = pa_facebookId;
            return this;
        }

        public Builder () {}

        public User build() {
            return new User(this);
        }
    }

    public User(Builder pa_builder) {
        id = UUID.randomUUID().toString();
        name = pa_builder.userName;
        setEmail(pa_builder.userEmail);
        facebookAccessToken = pa_builder.userFacebookAccessToken;
        facebookId = pa_builder.userFacebookId;
        userStatus = General.USER_PASSIVE_TYPE;
    }

    public String getUserKeyString() {
        return Key.create(User.class, id).getString();
    }
}

