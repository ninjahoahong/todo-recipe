package com.ninjahoahong.todorecipe.api.entities;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;
import com.ninjahoahong.todorecipe.api.helpers.Utilities;
import lombok.Getter;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Entity(name = "Recipe")
@NoArgsConstructor
public class Recipe {

    @Getter(AccessLevel.PUBLIC)
    @Id
    private String id;

    @Getter(AccessLevel.PUBLIC)
    @Index
    private String name;

    @Parent
    private Ref<User> author;

    public User getAuthor() {
        return author.get();
    }

    @Getter(AccessLevel.PUBLIC)
    @Index
    private Set<String> categoriesSet = new HashSet<String>();

    @Getter(AccessLevel.PUBLIC)
    private List<String> tasksList = new ArrayList<String>();

    @Getter(AccessLevel.PUBLIC)
    private String recipeColor;

    public Recipe addRecipeColor(String pa_recipeColor) {
        if(Utilities.isEmptyString(pa_recipeColor)){
            recipeColor = "#aaaaaa";
        } else {
            recipeColor = pa_recipeColor;
        }
        return this;
    }

    public Recipe(String pa_name, User pa_author, List<String> pa_tasksList, Set<String> pa_categoriesSet) {
        id = UUID.randomUUID().toString();
        name = pa_name;
        author = Ref.create(pa_author);
        tasksList = pa_tasksList;
        categoriesSet = pa_categoriesSet;
    }

    public String getRecipeKeyString() {
        return Key.create(author.getKey(), Recipe.class, id).getString();
    }
}
