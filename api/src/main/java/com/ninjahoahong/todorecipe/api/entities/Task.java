package com.ninjahoahong.todorecipe.api.entities;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Parent;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Entity(name = "Task")
@NoArgsConstructor
public class Task {

    @Getter(AccessLevel.PUBLIC)
    @Id
    private String id;

    @Getter(AccessLevel.PUBLIC)
    private String taskDescription;

    @Parent
    private Ref<Recipe> containerRecipe;

    public Recipe getContainerRecipe() {
        return containerRecipe.get();
    }

    public Task(String pa_description, Recipe pa_recipe) {
        id = UUID.randomUUID().toString();
        taskDescription = pa_description;
        containerRecipe = Ref.create(pa_recipe);
    }

    public String getTaskKeyString() {
        return Key.create(containerRecipe.getKey(), Task.class, id).getString();
    }
}
