package com.ninjahoahong.todorecipe.api.entities;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Entity(name = "Category")
@NoArgsConstructor
public class Category {
    @Id
    @Getter(AccessLevel.PUBLIC)
    private String name;

    @Getter(AccessLevel.PUBLIC)
    private String parentCategoryName;

    @Getter(AccessLevel.PUBLIC)
    private Set<String> recipeKeyStringsSet;

    public Category addRecipeColor(String pa_recipeKeyString) {
        recipeKeyStringsSet.add(pa_recipeKeyString);
        return this;
    }

    public void setParentCategoryName(String pa_parentCategoryName) {
        parentCategoryName = pa_parentCategoryName.trim().toLowerCase();
    }

    public Category(String pa_name) {
        name = pa_name.trim().toLowerCase();
        parentCategoryName = "root";
        recipeKeyStringsSet = new HashSet<String>();
    }

    public String getCategoryKeyString() {
        return Key.create(Category.class, name).getString();
    }
}
