package com.ninjahoahong.todorecipe.api.services;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

import com.ninjahoahong.todorecipe.api.entities.Category;
import com.ninjahoahong.todorecipe.api.entities.Recipe;
import com.ninjahoahong.todorecipe.api.entities.Task;
import com.ninjahoahong.todorecipe.api.entities.User;

public class OfyService {

    static {
        ObjectifyService.register(User.class);
        ObjectifyService.register(Task.class);
        ObjectifyService.register(Category.class);
        ObjectifyService.register(Recipe.class);
    }

    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }
}
