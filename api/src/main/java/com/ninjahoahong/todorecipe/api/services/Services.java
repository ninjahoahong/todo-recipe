package com.ninjahoahong.todorecipe.api.services;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.response.ConflictException;
import com.google.api.server.spi.response.UnauthorizedException;
import com.googlecode.objectify.Work;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Version;
import com.ninjahoahong.todorecipe.api.constants.General;
import com.ninjahoahong.todorecipe.api.constants.Messages;
import com.ninjahoahong.todorecipe.api.entities.Category;
import com.ninjahoahong.todorecipe.api.entities.Recipe;
import com.ninjahoahong.todorecipe.api.entities.Task;
import com.ninjahoahong.todorecipe.api.entities.User;
import com.ninjahoahong.todorecipe.api.forms.Forms;
import com.ninjahoahong.todorecipe.api.helpers.Utilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Api(
    name = "todorecipe",
    version = "v1"
)
public class Services {
    private static final Logger logger = LoggerFactory.getLogger(Services.class);

    @ApiMethod(
            name = "login.facebook",
            path = "users/oauth/facebook",
            httpMethod = ApiMethod.HttpMethod.POST
    )
    public User loginWithFacebook(@Named("facebookAccessToken") final String pa_facebookAccessToken)
            throws ConflictException, UnauthorizedException {
        final FacebookClient facebookClient = new DefaultFacebookClient(pa_facebookAccessToken, Version.LATEST);
        final com.restfb.types.User locFbUser = facebookClient.fetchObject("me", com.restfb.types.User.class);
        final User userByFacebookId = Utilities.findUserByFacebookId(locFbUser.getId());
        User locResult = OfyService.ofy().transact(new Work<User>() {
            @Override
            public User run() {
                try {
                    User locNewUser = userByFacebookId;
                    if(userByFacebookId == null){
                        locNewUser = new User.Builder()
                                .addAccessToken(pa_facebookAccessToken)
                                .addUserEmail(locFbUser.getEmail())
                                .addUserName(locFbUser.getName())
                                .addUserFacebookId(locFbUser.getId())
                                .build();
                        locNewUser.setUserStatus(General.USER_ACTIVE_TYPE);
                        Utilities.saveUser(locNewUser);
                        return locNewUser;
                    } else {
                        locNewUser.setFacebookAccessToken(pa_facebookAccessToken);
                        Utilities.saveUser(locNewUser);
                        return locNewUser;
                    }
                } catch (Exception pa_e) {
                    logger.error("Cannot login User");
                    logger.error(pa_e.getLocalizedMessage());
                    return null;
                }
            }
        });
        return locResult;
    }

    @ApiMethod(
            name = "users.get",
            path = "users/{userKeyString}",
            httpMethod = ApiMethod.HttpMethod.GET
    )
    public User getUserByKey(@Named("userKeyString") final String pa_key) {
        User result = OfyService.ofy().transact(new Work<User>() {
            @Override
            public User run() {
                try {
                    User user = Utilities.findUserByKeyString(pa_key);
                    return user;
                } catch (Exception pa_e) {
                    logger.error(pa_e.getLocalizedMessage());
                    return null;
                }
            }
        });
        return result;
    }

    @ApiMethod(
            name = "users.delete",
            path = "users/{userKeyString}/delete",
            httpMethod = ApiMethod.HttpMethod.POST
    )
    public Forms.DeleteResponseForm deleteUser(@Named("userKeyString") final String pa_key) {
        Forms.DeleteResponseForm result = OfyService.ofy().transact(new Work<Forms.DeleteResponseForm>() {
            @Override
            public Forms.DeleteResponseForm run() {
                try {
                    Utilities.delete(pa_key);
                    return new Forms.DeleteResponseForm(true, Messages.USER_DELETE_SUCCESS_MESSAGE);
                } catch (Exception pa_e) {
                    logger.error(pa_e.getLocalizedMessage());
                    return new Forms.DeleteResponseForm(false, Messages.USER_DELETE_FAIL_MESSAGE);
                }
            }
        });
        return result;
    }

    @ApiMethod(
            name = "recipes.create",
            path = "recipes",
            httpMethod = ApiMethod.HttpMethod.POST
    )
    public Recipe createRecipe(final Forms.RecipeCreationForm recipeCreationForm) {
        final Recipe result = OfyService.ofy().transact(new Work<Recipe>() {
            @Override
            public Recipe run() {
                try {
                    User locAuthor = Utilities.findUserByKeyString(recipeCreationForm.getAuthorKeyString());
                    Recipe newRecipe = new Recipe(
                            recipeCreationForm.getRecipeName(),
                            locAuthor,
                            recipeCreationForm.getTasksList(),
                            recipeCreationForm.getCategoriesSet()
                    ).addRecipeColor(recipeCreationForm.getRecipeColor());
                    Utilities.saveRecipe(newRecipe);
                    // Put the task into the category.
                    for( String categoryName : recipeCreationForm.getCategoriesSet()){
                        Category category = Utilities.findCategoryByName(categoryName);
                        if (category == null) {
                            category = new Category(categoryName);
                        }
                        category = category.addRecipeColor(newRecipe.getRecipeKeyString());
                        Utilities.saveCategory(category);
                    }
                    Utilities.saveRecipe(newRecipe);
                    for ( String taskDescription : recipeCreationForm.getTasksList()){
                        Task newTask = new Task(taskDescription, newRecipe);
                        Utilities.saveTask(newTask);
                    }
                    return newRecipe;
                } catch (Exception pa_e) {
                    logger.error(pa_e.getLocalizedMessage());
                    return null;
                }
            }
        });
        return result;
    }

    @ApiMethod(
            name = "recipes.getAllRecipesForAUser",
            path = "recipes",
            httpMethod = ApiMethod.HttpMethod.GET
    )
    public List<Recipe> getAllRecipesForAUser(@Named("userKeyString") final String userKeyString) {
        User author = Utilities.findUserByKeyString(userKeyString);
        List<Recipe> recipes = OfyService.ofy().load().type(Recipe.class).ancestor(author).list();
        return recipes;
    }

    @ApiMethod(
            name = "recipes.get",
            path = "recipes/{recipeKeyString}",
            httpMethod = ApiMethod.HttpMethod.GET
    )
    public Recipe getRecipeByKey(@Named("recipeKeyString") final String recipeKeyString) {
        Recipe result = OfyService.ofy().transact(new Work<Recipe>() {
            @Override
            public Recipe run() {
                try {
                    Recipe recipe = Utilities.findRecipeByKeyString(recipeKeyString);
                    return recipe;
                } catch (Exception pa_e) {
                    logger.error(pa_e.getLocalizedMessage());
                    return null;
                }
            }
        });
        return result;
    }

    @ApiMethod(
            name = "recipes.delete",
            path = "recipes/{receipeKeyString}/delete",
            httpMethod = ApiMethod.HttpMethod.POST
    )
    public Forms.DeleteResponseForm deleteRecipe(@Named("receipeKeyString") final String recipeKeyString) {
        Forms.DeleteResponseForm result = OfyService.ofy().transact(new Work<Forms.DeleteResponseForm>() {
            @Override
            public Forms.DeleteResponseForm run() {
                try {
                    Utilities.delete(recipeKeyString);
                    return new Forms.DeleteResponseForm(true, Messages.RECEIPT_DELETE_SUCCESS_MESSAGE);
                } catch (Exception pa_e) {
                    logger.error(pa_e.getLocalizedMessage());
                    return new Forms.DeleteResponseForm(false, Messages.RECEIPT_DELETE_FAIL_MESSAGE);
                }
            }
        });
        return result;
    }

    @ApiMethod(
            name = "category.create",
            path = "categories",
            httpMethod = ApiMethod.HttpMethod.POST
    )
    public Category getCategory(final Forms.CategoryCreationForm pa_categoryCreationForm) {
        Category result = OfyService.ofy().transact(new Work<Category>() {
            @Override
            public Category run() {
                try {
                    Category category = Utilities.findCategoryByName(pa_categoryCreationForm.getName());
                    if (category == null) {
                        logger.info("category cannot be found");
                        category = new Category(pa_categoryCreationForm.getName());
                        logger.info(pa_categoryCreationForm.getParentCategoryName());
                        if (pa_categoryCreationForm.getParentCategoryName() != null){
                            if (!Utilities.isEmptyString(pa_categoryCreationForm.getParentCategoryName())){
                                logger.info("non-root parent");
                                category.setParentCategoryName(pa_categoryCreationForm.getParentCategoryName());
                            }
                        }
                        Utilities.saveCategory(category);
                    }
                    return category;
                } catch (Exception e) {
                    logger.error(e.getLocalizedMessage());
                    return null;
                }
            }
        });
        return result;
    }

    @ApiMethod(
            name = "categories.getAllCategories",
            path = "categories",
            httpMethod = ApiMethod.HttpMethod.GET
    )
    public List<Category> getAllCategories() {
        List<Category> categories = OfyService.ofy().load().type(Category.class).list();
        return categories;
    }

    @ApiMethod(
            name = "categories.getRecipesInACategory",
            path = "categories/{categoryKeyString}/recipes",
            httpMethod = ApiMethod.HttpMethod.GET
    )
    public Set<Recipe> getRecipesInACategory(@Named("categoryKeyString") String pa_categoryKeyString) {
        Category category = Utilities.findCategoryByKeyString(pa_categoryKeyString);
        Set<Recipe> recipes = new HashSet<Recipe>();
        Set<String> recipeKeyStringsSet = category.getRecipeKeyStringsSet();
        for (String recipeKeyString : recipeKeyStringsSet) {
            Recipe recipe = Utilities.findRecipeByKeyString(recipeKeyString);
            recipes.add(recipe);
        }
        return recipes;
    }
}

