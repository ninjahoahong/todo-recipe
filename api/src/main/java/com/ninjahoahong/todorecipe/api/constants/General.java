package com.ninjahoahong.todorecipe.api.constants;


public class General {
    public static final String USER_PASSIVE_TYPE = "passive";
    public static final String USER_ACTIVE_TYPE = "active";
    public static final String DEFAULT_EMAIL= "default_email";
}
